<!DOCTYPE html>
<html lang="en">
<head>
	<title>Clinica- login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
<style>

.error{
  color:red;
}

</style>
</head>
<body>

<!-- Formulario de inicio de sesión -->	
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<span class="login100-form-title">
					CENTRO MEDICO ADSI VIRTUAL
				</span>
				<div class="login100-pic js-tilt" data-tilt>
					<img src="images/img-05.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" action="" method="post">
					<span class="login100-form-title">
						Inicio de sesión
					</span>

					<div class="wrap-input100 validate-input" data-validate = "El DNI es requerido">
						<input class="input100" type="text" name="dni" placeholder="DNI">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Contraseña es requerida">
						<input class="input100" type="password" name="pass" placeholder="Contraseña">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" name="ingresar">
							Login
						</button>
					</div>

					
					<div class="text-center p-t-136">

<!-- Final del formulario de inicio de sesión -->						

<?php

/* Conexion base de datos */

 	$servername = "localhost";
	$username = "Administrador";
	$password = "Administrador";
	$dbname = "consultas";

  	$conn = mysqli_connect($servername, $username, $password,$dbname);

	if (!$conn) {
	   die("Connection failed: " . mysqli_connect_error());
	    
	}

	if (isset($_POST["ingresar"])){

	  	$usuario=$_POST["dni"];
    	$pass=$_POST["pass"];

    	$sql = "SELECT usuLogin,usutipo FROM usuarios WHERE dniUsu = '$usuario' AND usuPassword='$pass'";
        
        $result1 = mysqli_query ($conn, $sql);

        if(mysqli_num_rows($result1) > 0){
      
	        session_start();

	        $registro = mysqli_fetch_row($result1);
	        $_SESSION['rol']=$registro[1];
	        
	      
	        $_SESSION['usuario']=$registro[0];

        	if($_SESSION['rol']=="Medico" || $_SESSION['rol']=="Paciente"){

        		$_SESSION['dni']=$usuario;
        	}

        	if($_SESSION['rol']=="Administrador"){

        		header("Location: portfolio/administrador.php");
        	}

        	if($_SESSION['rol']=="Medico"){

        		header("Location: portfolio/medico.php");
        		
        	}

        	if($_SESSION['rol']=="Asistente"){

        		header("Location: portfolio/asistente.php");
        		
        	}

        	if($_SESSION['rol']=="Paciente"){
        		
        		header("Location: portfolio/paciente.php");
        		
        	}
           
      	}else{

      		echo "<b class='error'> Datos de inicio de sesión incorrectos </b>";
      	}
		
	}
	mysqli_close($conn);
?>

					</div>
				</form>
			</div>
		</div>
	</div>



<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>