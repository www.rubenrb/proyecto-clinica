<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
     <!-- Site Metas -->
    <title>Clinica</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
    <!-- Site Icons -->
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="../css/pogo-slider.min.css">
  <!-- Site CSS -->
    <link rel="stylesheet" href="../css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>

  .centro{
    display: flex;
    justify-content: center;
  }

  .animate
{
  transition: all 0.1s;
  -webkit-transition: all 0.1s;
}

.action-button
{
  position: relative;
  padding: 10px 40px;
  margin: 0px 10px 10px 0px;
  float: left;
  border-radius: 3px;
  font-family: 'Lato', sans-serif;
  font-size: 18px;
  color: #FFF;
  text-decoration: none;  
}

.red
{
  background-color: #e74c3c;
  border-bottom: 5px solid #c0392b;
  text-shadow: 0px -2px #c0392b;
}

.action-button:active
{
  transform: translate(0px,5px);
  -webkit-transform: translate(0px,5px);
  border-bottom: 1px solid;
}
.input-group-addon{
  padding-right:20px;
}

.form-control{
      font-size: 1.5rem;
}
#anadirpaciente{
  margin-left:20%;
}
.error{
  color:red;
}

</style>

</head>


<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

<?php
 session_start();
 if(isset($_SESSION["usuario"]) && ($_SESSION["rol"]=="Administrador")) {


  function test_input($data) {
      $data = trim($data); 
      $data = stripslashes($data); 
      return $data;
    }

  if (isset($_POST['salir'])){
      session_destroy();
      header("location:../login.php");
  }

  if (isset($_POST['panel'])){
          header("Location:administrador.php");
  }
  
  if (!isset($_POST['paciente']) && !isset($_POST['medico']) && (!isset($_POST["anadirpaciente"])) && (!isset($_POST["anadirmedico"]))){
    
  ?>

<!-- Panel de control del administrador --> 

    <form action="" method="post">  
      <div id="services" class="services-box">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <div class="title-box">
                  <h2>Buenos días <?php echo ($_SESSION['usuario']);?>, se ha validado como "<?php echo ($_SESSION['rol']);?>"</h2>
                  <p>Panel de control para usuarios del rol de administrador</p>
                </div>
              </div>
            </div>

          <div class="container">
            <div class="card-deck mb-3 text-center">
              <div class="card mb-4 box-shadow">
                <div class="card-header">
                  <h1 class="my-0 font-weight-normal">Alta paciente</h1>
                </div>
                <div class="serviceBox">
                  <div class="service-icon"><i class="fa fa-heart" aria-hidden="true"></i></div>
                  <div class="card-body">
                                        
                    <button class="btn btn-lg btn-block btn-outline-primary" name="paciente">Acceder</button>
                  </div>
                </div>
              </div>
               <div class="card mb-4 box-shadow">
                <div class="card-header">
                  <h1 class="my-0 font-weight-normal">Alta medico</h1>
                </div>
                <div class="serviceBox">
                  <div class="service-icon"><i class="fa fa-stethoscope" aria-hidden="true"></i></div>
                  <div class="card-body">
                                        
                    <button class="btn btn-lg btn-block btn-outline-primary" name="medico">Acceder</button>
                  </div>
                </div>
              </div>
              
            </div>
            <div class="centro">
              <button class="action-button shadow animate red"  name="salir">Cerrar sesión</button>
            </div>           
          </div>
        </div>
      </div>  
    </form>

<!-- Final del panel de control del administrador --> 

<?php

  }

    if (isset($_POST['paciente']) || isset($_POST['medico']) || isset($_POST['anadirpaciente']) || isset($_POST['anadirmedico'])){ /* Modulos para las diferentes opciones del administrador */

      /* Conexion base de datos */

      $servername = "localhost";
      $username = "Administrador";
      $password = "Administrador";
      $dbname = "consultas";


      $conn = mysqli_connect($servername, $username, $password,$dbname);

      if (!$conn){
        die("Connection failed: " . mysqli_connect_error());
      
      }

      if (isset($_POST['paciente']) || isset($_POST['anadirpaciente'])){ /* Modulo para dar de alta un nuevo paciente */

          $apelErr=$nameErr = $passError = "";
          $apel=$name =$contra="";
          $apel1=$name1=$contra3=false;


        if (isset($_POST["anadirpaciente"])) { /* Validaciones del formulario que añade un nuevo paciente */

          $contra1=$_POST["pass"];
          $contra2=$_POST["rpass"];

          
            if ($contra1!=$contra2){

              $passError = "* Las contraseñas no coinciden";

            }else{

               $contra3 = true;
            }

              $name = test_input($_POST["nombre"]);

              if (!preg_match("/^[a-zA-Z]*$/",$name)){

                  $nameErr = "<br>* Solo se permiten letras en el nombre";

              }else{

                  $name1=true;
              }

              if (strlen($name) > 12 ){

                  $nameErr = "<br>* El nombre no puede exceder los 12 caracteres";

              }else{

                  $name1=true;
              }


               $apel = test_input($_POST["apellidos"]);

              if (strlen($apel) > 30 ){

                  $apelErr = "<br>* Los apellidos no pueden exceder los 30 caracteres";

              }else{

                  $apel1=true;
              }       
        }  /* Final de las validaciones */ 

?>
<!-- Formulario para añadir un nuevo paciente --> 

            <div class="container">
            <div class="well form-horizontal"  id="contact_form">
            <fieldset>      

            <form action="" method="post" >
             
                <br><br>
  
              <center><h2><b>Alta de paciente</b></h2></center><br>

              <div class="form-group">
                <label class="col-md-4 control-label">DNI</label>  
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input  name="dni" placeholder="DNI" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label" >Nombre</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input name="nombre" placeholder="Nombre" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>

               <div class="form-group">
                <label class="col-md-4 control-label" >Apellidos</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input name="apellidos" placeholder="Apellidos" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label">Fecha</label>  
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                <input  name="fecha"class="form-control" type="date" value="0000-00-00" min="1900-01-01" max="2100-12-31" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label" >Contraseña</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-cog"></i></span>
                <input name="pass" placeholder="Contraseña" class="form-control"  type="password" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label" >Confirmar contraseña</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-cog"></i></span>
                <input name="rpass" placeholder="Confirmar contraseña" class="form-control"  type="password" required>
                  </div>
                </div>
              </div>

              <div class="form-group"> 
                <label class="col-md-4 control-label">Sexo</label>
                  <div class="col-md-4 selectContainer">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                  <select name="sex" class="form-control selectpicker" required>
                    <option value="">Selecciona el sexo del paciente</option>
                    <option value="Masculino">Masculino</option>
                    <option value="Femenino">Femenino</option>
               
                  </select>
                </div>
              </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4"><br>
                   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="anadirpaciente" class="btn btn-success" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAÑADIR <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
                 
              </div><br>
              <span class="error"> <?php echo $passError;?></span><span class="error"> <?php echo $nameErr;?></span><span class="error"> <?php echo $apelErr;?></span></p><br>
    

<?php    

        if(($apel1==true) && ($name1==true) && ($contra3==true)){ /* Si todas las validaciones son aceptadas procede a trabajar con la base de datos */

          if (isset($_POST['anadirpaciente'])){

            $dni=$_POST['dni'];
            $nombre=$_POST['nombre'];
            $apellidos=$_POST['apellidos'];
            $pass=$_POST['pass'];
            $fecha=$_POST['fecha'];
            $sexo=$_POST['sex'];

            $sql = "SELECT dniUsu FROM usuarios WHERE dniUsu = '$dni'";
        
            $result1 = mysqli_query ($conn, $sql);


            if(mysqli_num_rows($result1) > 0){

              echo "<span class='error'>* Usuario ya existente.</span";

            }else{
            
              $sql1 = "INSERT INTO pacientes (dniPac, pacNombres, pacApellidos, pacFechaNacimiento, pacSexo) VALUES ('$dni', '$nombre', '$apellidos', '$fecha', '$sexo')";
              $sql2 = "INSERT INTO usuarios (dniUsu, usuLogin, usuPassword, usuEstado, usutipo) VALUES ('$dni', '$nombre', '$pass', 'Activo', 'Paciente')";

              if ((mysqli_query($conn, $sql1)) && (mysqli_query($conn, $sql2))) {
                
?>
                  <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Se ha añadido el paciente con exito.</div>  <!-- Mensaje de exito si se ha añadido a la base de datos -->

                    
<?php 
              }else{

                echo "<br><p> Error al añadir un nuevo paciente </p>";

              }
            }
          }
        }


?>        
          </form>
            <form action="" method="post">
                  <div class="col-md-4"><br>
                   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
            </form>
           <fieldset>
            </div>  
            </div>

<!--Final del formulario para añadir un nuevo paciente --> 

<?php               

      } /* Final del modulo que añade nuevos pacientes */
        
      if (isset($_POST['medico']) || isset($_POST['anadirmedico'])){ /* Modulo para dar de alta un nuevo medico */

          $tlfErr=$EmailErr=$apelErr=$nameErr = $passError = "";
          $tlf=$Email=$apel=$name =$contra="";
          $tlf1=$Email1=$apel1=$name1=$contra1=false;


        if (isset($_POST["anadirmedico"])) {  /* Validaciones del formulario que añade un nuevo medico */

          $contra1=$_POST["pass"];
          $contra2=$_POST["rpass"];
          $Email=$_POST["email"];
          $tlf=$_POST["tlf"];

          
            if ($contra1!=$contra2) {

              $passError = "* Las contraseñas no coinciden";

            }else{

               $contra1 = true;
            }

              $name = test_input($_POST["nombre"]);

              if (!preg_match("/^[a-zA-Z]*$/",$name)){

                  $nameErr = "<br>* Solo se permiten letras en el nombre";

              }else{

                  $name1=true;
              }

              if (strlen($name) > 12 ){

                  $nameErr = "<br>* El nombre no puede exceder los 12 caracteres";

              }else{

                  $name1=true;
              }


               $apel = test_input($_POST["apellidos"]);

              if (strlen($apel) > 30 ){

                  $apelErr = "<br>* Los apellidos no pueden exceder los 30 caracteres";

              }else{

                  $apel1=true;
              }

              if (filter_var($Email, FILTER_VALIDATE_EMAIL)){

                $Email1=true;

              }else{

                $EmailErr= "<br>* Formato de mail no valido";
              }

              if (preg_match('/^[0-9]{9,9}$/', $tlf)){

                $tlf1=true;

              }else{

                $tlfErr= "<br>* Formato de telefono no valido";
              }
                 
        }  /* Final de las validaciones */ 

?>
<!-- Formulario para añadir un nuevo medico --> 

        <div class="container">

          <form class="well form-horizontal" action="" method="post"  id="contact_form">
              <fieldset>
                <br><br>

              <center><h2><b>Alta de medico</b></h2></center><br>

              <div class="form-group">
                <label class="col-md-4 control-label" >Nombre</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input name="nombre" placeholder="Nombre" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>

               <div class="form-group">
                <label class="col-md-4 control-label" >Apellidos</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input name="apellidos" placeholder="Apellidos" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>

               <div class="form-group">
                <label class="col-md-4 control-label" >Especialidad</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-briefcase"></i></span>
                <input name="espec" placeholder="Especialidad" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>      

             <div class="form-group">
                <label class="col-md-4 control-label">Teléfono</label>  
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                <input  name="tlf" placeholder="Telefono" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label">Email</label>  
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <input  name="email" placeholder="Email" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label">DNI</label>  
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input  name="dni" placeholder="DNI" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label" >Contraseña</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-cog"></i></span>
                <input name="pass" placeholder="Contraseña" class="form-control"  type="password" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label" >Confirmar contraseña</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-cog"></i></span>
                <input name="rpass" placeholder="Confirmar contraseña" class="form-control"  type="password" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4"><br>
                   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="anadirmedico" class="btn btn-success" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAÑADIR <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
              </div><br>
              <span class="error"> <?php echo $passError;?></span><span class="error"> <?php echo $nameErr;?></span><span class="error"> <?php echo $apelErr;?></span><span class="error"> <?php echo $EmailErr;?></span></span><span class="error"> <?php echo $tlfErr;?></span></p><br>
         
<?php    

        if(($apel1==true) && ($name1==true) && ($contra1==true)  && ($Email1==true)  && ($tlf1==true)){ /* Si todas las validaciones son aceptadas procede a trabajar con la base de datos */

          if (isset($_POST['anadirmedico'])){

            $dni=$_POST['dni'];
            $nombre=$_POST['nombre'];
            $apellidos=$_POST['apellidos'];
            $pass=$_POST['pass'];
            $espec=$_POST['espec'];
            $tel=$_POST['tlf'];
            $email=$_POST['email'];

            $sql = "SELECT dniUsu FROM usuarios WHERE dniUsu = '$dni'";
        
            $result1 = mysqli_query ($conn, $sql);

            if(mysqli_num_rows($result1) > 0){

              echo "<span class='error'>* Usuario ya existente.</span";

            }else{
            
              $sql1 = "INSERT INTO medicos (dniMed, medNombres, medApellidos, medEspecialidad, medTelefono, medCorreo) VALUES ('$dni', '$nombre', '$apellidos', '$espec', '$tlf', '$email')";
              $sql2 = "INSERT INTO usuarios (dniUsu, usuLogin, usuPassword, usuEstado, usutipo) VALUES ('$dni', '$nombre', '$pass', 'Activo', 'Medico')";

              if ((mysqli_query($conn, $sql1)) && (mysqli_query($conn, $sql2))) {
                
?>
                  <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Se ha añadido el medico con exito.</div> <!-- Mensaje de exito si se ha añadido a la base de datos -->
                   
<?php 
              }else{

                echo "<br><p> Error al añadir un nuevo medico </p>";

              }
            }
          }
        }

?>             
          </form>
            <form action="" method="post">
                  <div class="col-md-4"><br>
                   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
            </form>
          </div>
          <fieldset>
          </div>  
        </div>
<!--Final del formulario para añadir un nuevo paciente --> 

<?php               

      } /* Final del modulo que añade nuevos pacientes */
      mysqli_close($conn);
    }
}

  
?>
  

  <!-- ALL JS FILES -->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
    <!-- ALL PL../UGINS -->
  <script src="../js/jquery.magnific-popup.min.js"></script>
    <script src="../js/jquery.pogo-slider.min.js"></script> 
  <script src="../js/slider-index.js"></script>
  <script src="../js/smoothscroll.js"></script>
  <script src="../js/TweenMax.min.js"></script>
  <script src="../js/main.js"></script>
  <script src="../js/owl.carousel.min.js"></script>
  <script src="../js/form-validator.min.js"></script>
    <script src="../js/contact-form-script.js"></script>
  <script src="../js/isotope.min.js"></script> 
  <script src="../js/images-loded.min.js"></script>  
    <script src="../js/custom.js"></script>

    
</body>
</html>