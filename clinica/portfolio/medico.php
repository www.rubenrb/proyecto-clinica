<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
     <!-- Site Metas -->
    <title>Health Lab - Responsive HTML5 Template</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
 
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="../css/pogo-slider.min.css">
  <!-- Site CSS -->
    <link rel="stylesheet" href="../css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>

  .centro{
    display: flex;
    justify-content: center;
  }

  .animate
{
  transition: all 0.1s;
  -webkit-transition: all 0.1s;
}

.action-button
{
  position: relative;
  padding: 10px 40px;
  margin: 0px 10px 10px 0px;
  float: left;
  border-radius: 3px;
  font-family: 'Lato', sans-serif;
  font-size: 18px;
  color: #FFF;
  text-decoration: none;  
}

.red
{
  background-color: #e74c3c;
  border-bottom: 5px solid #c0392b;
  text-shadow: 0px -2px #c0392b;
}

.action-button:active
{
  transform: translate(0px,5px);
  -webkit-transform: translate(0px,5px);
  border-bottom: 1px solid;
}
.input-group-addon{
  padding-right:20px;
}

.form-control{
      font-size: 1.5rem;
}
#anadirpaciente{
  margin-left:20%;
}
.error{
  color:red;
}

table {
  background: #f5f5f5;
  border-collapse: separate;
  box-shadow: inset 0 1px 0 #fff;
  font-size: 16px;
  line-height: 24px;
  margin: 30px auto;
  text-align: center;
  width: 1050px;
} 

th {
  background: url(https://jackrugile.com/images/misc/noise-diagonal.png), linear-gradient(#777, #444);
  border-left: 1px solid #555;
  border-right: 1px solid #777;
  border-top: 1px solid #555;
  border-bottom: 1px solid #333;
  box-shadow: inset 0 1px 0 #999;
  color: #fff;
  font-weight: bold;
  padding: 10px 15px;
  position: relative;
  text-shadow: 0 1px 0 #000;  
}

th:after {
  background: linear-gradient(rgba(255,255,255,0), rgba(255,255,255,.08));
  content: '';
  display: block;
  height: 25%;
  left: 0;
  margin: 1px 0 0 0;
  position: absolute;
  top: 25%;
  width: 100%;
}

th:first-child {
  border-left: 1px solid #777;  
  box-shadow: inset 1px 1px 0 #999;
}

th:last-child {
  box-shadow: inset -1px 1px 0 #999;
}

td {
  border-right: 1px solid #fff;
  border-left: 1px solid #e8e8e8;
  border-top: 1px solid #fff;
  border-bottom: 1px solid #e8e8e8;
  padding: 10px 15px;
  position: relative;
  transition: all 300ms;
}

td:first-child {
  box-shadow: inset 1px 0 0 #fff;
} 

td:last-child {
  border-right: 1px solid #e8e8e8;
  box-shadow: inset -1px 0 0 #fff;
} 


tr:last-of-type td {
  box-shadow: inset 0 -1px 0 #fff; 
}

tr:last-of-type td:first-child {
  box-shadow: inset 1px -1px 0 #fff;
} 

tr:last-of-type td:last-child {
  box-shadow: inset -1px -1px 0 #fff;
} 

tbody:hover td {
  color: transparent;
  text-shadow: 0 0 3px #aaa;
}

tbody:hover tr:hover td {
  color: #444;
  text-shadow: 0 1px 0 #fff;
}


</style>


</head>


<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

<?php
 session_start();
if(isset($_SESSION["usuario"]) && ($_SESSION["rol"]=="Medico")){

    $dni=$_SESSION['dni'];

    if (isset($_POST['salir'])){
      session_destroy();
      header("location:../login.php");
    }

    if (isset($_POST['panel'])){
          header("Location:medico.php");
    }

    if (isset($_POST['asignar'])){
          header("Location:medico.php");
    }

    if (!isset($_POST['vercitasA']) && !isset($_POST['vercitasP']) && (!isset($_POST["verpacientes"])) && (!isset($_POST["atender"])) && (!isset($_POST["asignar"]))){ 
  
?>
<!-- Panel de control del medico --> 

      <form action="" method="post">  
        <div id="services" class="services-box">
            <div class="container">
              <div class="row">
                <div class="col-lg-12">
                  <div class="title-box">
                    <h2>Buenos días <?php echo ($_SESSION['usuario']);?>, se ha validado como "<?php echo ($_SESSION['rol']);?>"</h2>
                    <p>Panel de control para usuarios del rol de medico</p>
                  </div>
                </div>
              </div>

            <div class="container">
              <div class="card-deck mb-3 text-center">
                <div class="card mb-4 box-shadow">
                  <div class="card-header">
                    <h1 class="my-0 font-weight-normal">Ver citas atendidas</h1>
                  </div>
                  <div class="serviceBox">
                    <div class="service-icon"><i class="fa fa-hospital-o" aria-hidden="true"></i></div>
                    <div class="card-body">
                      
                      
                      <button class="btn btn-lg btn-block btn-outline-primary" name="vercitasA">Acceder</button>
                    </div>
                  </div>
                </div>

                <div class="card mb-4 box-shadow">
                  <div class="card-header">
                    <h1 class="my-0 font-weight-normal">Ver citas pendientes</h1>
                  </div>
                  <div class="serviceBox">
                    <div class="service-icon"><i class="fa fa-ambulance" aria-hidden="true"></i></div>
                    <div class="card-body">
                      
                      
                      <button class="btn btn-lg btn-block btn-outline-primary" name="vercitasP">Acceder</button>
                    </div>
                  </div>
                </div>
                 <div class="card mb-4 box-shadow">
                  <div class="card-header">
                    <h1 class="my-0 font-weight-normal">Ver pacientes</h1>
                  </div>
                  <div class="serviceBox">
                    <div class="service-icon"><i class="fa fa-wheelchair" aria-hidden="true"></i></div>
                    <div class="card-body">
                      
                      
                      <button  class="btn btn-lg btn-block btn-outline-primary" name="verpacientes">Acceder</button>
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="centro">
                <button class="action-button shadow animate red"  name="salir">Cerrar sesión</button>
              </div>
              
            </div>
          </div>
        </div>  
      </form>

<!-- Final del panel de control del medico -->      

<?php

    }

    if (isset($_POST['vercitasA']) || isset($_POST['vercitasP']) || (isset($_POST["verpacientes"]))|| (isset($_POST["atender"]))|| (isset($_POST["asignar"]))){  /* Modulos para las diferentes opciones del medico */

      $servername = "localhost";
      $username = "Medico";
      $password = "Medico";
      $dbname = "consultas";

      $conn = mysqli_connect($servername, $username, $password,$dbname);

      if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
      
      }

      if (isset($_POST['vercitasA'])){ /* Modulo para ver las citas atendidas */

        $sql30 = " SELECT citas.citFecha, citas.citHora, pacientes.pacNombres, pacientes.pacApellidos, medicos.medNombres, medicos.medApellidos, consultorios.conNombre, citas.citEstado, citas.CitObservaciones FROM citas INNER JOIN pacientes ON citas.citPaciente= pacientes.dniPac INNER JOIN medicos ON citas.citMedico=medicos.dniMed INNER JOIN consultorios ON citas.citConsultorio= consultorios.idConsultorio WHERE citas.citEstado= 'Atendido' AND citas.citMedico= '$dni'" ;
        
        $result30 = mysqli_query ($conn, $sql30);

?>
        
        <div class="container">
        <div class="well form-horizontal"  id="contact_form">
        <fieldset>

                <center><h2><b>Lista de citas atendidas</b></h2></center><br>
          <table>
            <thead>
              <tr>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Paciente</th>
                <th>Medico</th>
                <th>Consultorio</th>
                <th>Estado</th>
                <th>Observaciones</th>
              </tr>
            </thead>
            <tbody>
<?php
            while ($registro30 = mysqli_fetch_row($result30)) {
?>
              <tr>
                <td><?php echo $registro30[0];?></td>
                <td><?php echo $registro30[1];?></td>
                <td><?php echo $registro30[2];?> <?php echo $registro30[3];?></td>
                <td><?php echo $registro30[4];?> <?php echo $registro30[5];?></td>
                <td><?php echo $registro30[6];?></td>
                <td><?php echo $registro30[7];?></td>
                <td><?php echo $registro30[8];?></td>
              </tr>
<?php
          }
?>
            
          </tbody>
        </table>
        <form action="" method="post">
                  <div class="col-md-4"><br>
                   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
            </form>
          </div>
      <fieldset>
      </div>  
      </div>

<?php
      } /* Final del modulo para ver las citas atendidas */

      if (isset($_POST['vercitasP']) || isset($_POST["asignar"])){ /* Modulo para ver las citas pendientes */
        
        $sql40 = " SELECT citas.citFecha, citas.citHora, pacientes.pacNombres, pacientes.pacApellidos, medicos.medNombres, medicos.medApellidos, consultorios.conNombre, citas.citEstado, citas.CitObservaciones, citas.idCita FROM citas INNER JOIN pacientes ON citas.citPaciente= pacientes.dniPac INNER JOIN medicos ON citas.citMedico=medicos.dniMed INNER JOIN consultorios ON citas.citConsultorio= consultorios.idConsultorio WHERE citas.citEstado= 'Asignado' AND citas.citMedico= '$dni'" ;
        
        $result40 = mysqli_query ($conn, $sql40);


?>
       
          <div class="container">
          <div class="well form-horizontal"  id="contact_form">
          <fieldset>

          <center><h2><b>Lista de citas atendidas</b></h2></center><br>
          <form action="" method="post">
            <table>
              <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Hora</th>
                  <th>Paciente</th>
                  <th>Medico</th>
                  <th>Consultorio</th>
                  <th>Estado</th>
                  <th>Observaciones</th>
                  <th>Atender</th>
                </tr>
              </thead>
              <tbody>
<?php
            while ($registro40 = mysqli_fetch_row($result40)) {
?>
                <tr>
                  <td><?php echo $registro40[0];?></td>
                  <td><?php echo $registro40[1];?></td>
                  <td><?php echo $registro40[2];?> <?php echo $registro40[3];?></td>
                  <td><?php echo $registro40[4];?> <?php echo $registro40[5];?></td>
                  <td><?php echo $registro40[6];?></td>
                  <td><?php echo $registro40[7];?></td>
                  <td><?php echo $registro40[8];?></td>
                  <td><input type="checkbox" name="checkList[]" value="<?php echo $registro40[9]?>"></td>
                </tr>
<?php
            }
?>
            
              </tbody>
            </table>
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4"><br>
                   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="atender" class="btn btn-success" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspATENDER <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
                 
              </div><br>
          </form>
          <form action="" method="post">
                    <div class="col-md-4"><br>
                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                  </div>
              </form>
            </div>
          <fieldset>
          </div>  
          </div>
      

<?php
      } /* Final del modulo para ver las citas atendidas */

        if (isset($_POST['atender'])) { /* Modulo asociado con el de citas pendientes para poder atender una cita */
          if (isset($_POST['checkList'])) {
      
              $cuenta=count($_POST['checkList']);

            if ($cuenta==1){

              foreach ($_POST['checkList'] as $selected) {
                        
?>  
                  <div class="container">
                  <div class="well form-horizontal"  id="contact_form">
                  <fieldset>
                    <center><h2><b>Observaciones del paciente</b></h2></center><br>
                    <form action="" method="post">
                      <div align = "center">
                        <textarea name="observacion" rows="4" cols="50"></textarea>
                        <input  name="cita" type="hidden" value="<?php echo $selected?>">
                      </div>

                      <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-4"><br>
                           &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="asignar" class="btn btn-success" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspASIGNAR <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                        </div>
                         
                      </div><br>
                    </form>
                    <form action="" method="post">
                            <div class="col-md-4"><br>
                             &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                          </div>
                      </form>
                  <fieldset>
                  </div>  
                  </div>

<?php   
              }
                     
            }else{

?> 
              <div class="container">
              <div class="well form-horizontal"  id="contact_form">
              <fieldset>
<?php
                echo "<center><h2><b>Escoge solo una cita para atender</b></h2></center><br>";
?>  
              <form action="" method="post">
                    <div class="col-md-4"><br>
                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                  </div>
              </form>
            <fieldset>
            </div>  
            </div>

<?php  
            }
          }else{

?> 
            <div class="container">
            <div class="well form-horizontal"  id="contact_form">
            <fieldset>

<?php
              echo "<center><h2><b>Debes escoger almenos una cita para atender</b></h2></center><br>";

?>  
              <form action="" method="post">
                    <div class="col-md-4"><br>
                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                  </div>
              </form>
            <fieldset>
            </div>  
            </div>

<?php  
          }

        }  /* Final del modulo para atender una cita */ 

        if (isset($_POST['asignar'])){ /* Modulo asociado con el de atender cita para poder actializar los datos de la cita en la base de datos */

                    $cita=$_POST["cita"];
                    $observacion=$_POST["observacion"];
          
                    $modif = " UPDATE citas SET citEstado='Atendido', CitObservaciones='$observacion' WHERE idCita='$cita' ";
                    $result1 = mysqli_query ($conn, $modif);

               if ($result1 == FALSE) {
                echo "Error al atender la cita.<br />";

              }else{
?> 
                <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Se ha añadido el paciente con exito.</div>
<?php  
              }
                        
          } /* Final del modulo para actualizar los datos de la cita atendida */ 


      if (isset($_POST['verpacientes'])){ /* Modulo para ver los paciente */

        $sql60 = " SELECT * FROM pacientes"; 

        $result60 = mysqli_query ($conn, $sql60);

?>

        <div class="container">
        <div class="well form-horizontal"  id="contact_form">
        <fieldset>

          <center><h2><b>Lista de pacientes</b></h2></center><br>
          <table>
            <thead>
              <tr>
                <th>DNI</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Fecha de nacimiento</th>
                <th>Sexo</th>
              </tr>
            </thead>
            <tbody>
<?php
          while ($registro60 = mysqli_fetch_row($result60)) {
?>
              <tr>
                <td><?php echo $registro60[0];?></td>
                <td><?php echo $registro60[1];?></td>
                <td><?php echo $registro60[2];?></td>
                <td><?php echo $registro60[3];?></td>
                <td><?php echo $registro60[4];?></td>
              </tr>
<?php
          }
?>
            
            </tbody>
          </table>
          <form action="" method="post">
                    <div class="col-md-4"><br>
                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                  </div>
              </form>
            </div>
        <fieldset>
        </div>  
        </div>

<?php
      } /* Final del modulo para ver los pacientes */

    }
}

  
?>
  


  <!-- ALL JS FILES -->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
    <!-- ALL PL../UGINS -->
  <script src="../js/jquery.magnific-popup.min.js"></script>
    <script src="../js/jquery.pogo-slider.min.js"></script> 
  <script src="../js/slider-index.js"></script>
  <script src="../js/smoothscroll.js"></script>
  <script src="../js/TweenMax.min.js"></script>
  <script src="../js/main.js"></script>
  <script src="../js/owl.carousel.min.js"></script>
  <script src="../js/form-validator.min.js"></script>
    <script src="../js/contact-form-script.js"></script>
  <script src="../js/isotope.min.js"></script> 
  <script src="../js/images-loded.min.js"></script>  
    <script src="../js/custom.js"></script>

    
</body>
</html>