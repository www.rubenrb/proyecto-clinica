<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
     <!-- Site Metas -->
    <title>Health Lab - Responsive HTML5 Template</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="../css/pogo-slider.min.css">
  <!-- Site CSS -->
    <link rel="stylesheet" href="../css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>

  .centro{
    display: flex;
    justify-content: center;
  }

  .animate
{
  transition: all 0.1s;
  -webkit-transition: all 0.1s;
}

.action-button
{
  position: relative;
  padding: 10px 40px;
  margin: 0px 10px 10px 0px;
  float: left;
  border-radius: 3px;
  font-family: 'Lato', sans-serif;
  font-size: 18px;
  color: #FFF;
  text-decoration: none;  
}

.red
{
  background-color: #e74c3c;
  border-bottom: 5px solid #c0392b;
  text-shadow: 0px -2px #c0392b;
}

.action-button:active
{
  transform: translate(0px,5px);
  -webkit-transform: translate(0px,5px);
  border-bottom: 1px solid;
}
.input-group-addon{
  padding-right:20px;
}

.form-control{
      font-size: 1.5rem;
}
#anadirpaciente{
  margin-left:20%;
}
.error{
  color:red;
}

table {
  background: #f5f5f5;
  border-collapse: separate;
  box-shadow: inset 0 1px 0 #fff;
  font-size: 16px;
  line-height: 24px;
  margin: 30px auto;
  text-align: center;
  width: 1050px;
} 

th {
  background: url(https://jackrugile.com/images/misc/noise-diagonal.png), linear-gradient(#777, #444);
  border-left: 1px solid #555;
  border-right: 1px solid #777;
  border-top: 1px solid #555;
  border-bottom: 1px solid #333;
  box-shadow: inset 0 1px 0 #999;
  color: #fff;
  font-weight: bold;
  padding: 10px 15px;
  position: relative;
  text-shadow: 0 1px 0 #000;  
}

th:after {
  background: linear-gradient(rgba(255,255,255,0), rgba(255,255,255,.08));
  content: '';
  display: block;
  height: 25%;
  left: 0;
  margin: 1px 0 0 0;
  position: absolute;
  top: 25%;
  width: 100%;
}

th:first-child {
  border-left: 1px solid #777;  
  box-shadow: inset 1px 1px 0 #999;
}

th:last-child {
  box-shadow: inset -1px 1px 0 #999;
}

td {
  border-right: 1px solid #fff;
  border-left: 1px solid #e8e8e8;
  border-top: 1px solid #fff;
  border-bottom: 1px solid #e8e8e8;
  padding: 10px 15px;
  position: relative;
  transition: all 300ms;
}

td:first-child {
  box-shadow: inset 1px 0 0 #fff;
} 

td:last-child {
  border-right: 1px solid #e8e8e8;
  box-shadow: inset -1px 0 0 #fff;
} 


tr:last-of-type td {
  box-shadow: inset 0 -1px 0 #fff; 
}

tr:last-of-type td:first-child {
  box-shadow: inset 1px -1px 0 #fff;
} 

tr:last-of-type td:last-child {
  box-shadow: inset -1px -1px 0 #fff;
} 

tbody:hover td {
  color: transparent;
  text-shadow: 0 0 3px #aaa;
}

tbody:hover tr:hover td {
  color: #444;
  text-shadow: 0 1px 0 #fff;
}



</style>

</head>


<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

<?php
 session_start();
if(isset($_SESSION["usuario"]) && ($_SESSION["rol"]=="Asistente")) {

  function test_input($data) {
      $data = trim($data); 
      $data = stripslashes($data); 
      return $data;
    }

  if (isset($_POST['salir'])){
      session_destroy();
      header("location:../login.php");
    }

  if (isset($_POST['panel'])){
          header("Location:asistente.php");
  }

  if (!isset($_POST['vercitas']) && !isset($_POST['nuevacita']) && (!isset($_POST["nuevopaciente"])) && (!isset($_POST["verpaciente"]))  && (!isset($_POST["anadircita"])) && (!isset($_POST["anadirpaciente"]))){ 
 
?>

<!-- Panel de control del asistente --> 

    <form action="" method="post">  
      <div id="services" class="services-box">
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <div class="title-box">
                  <h2>Buenos días <?php echo ($_SESSION['usuario']);?>, se ha validado como "<?php echo ($_SESSION['rol']);?>"</h2>
                  <p>Panel de control para usuarios del rol de administrador</p>
                </div>
              </div>
            </div>

            <div class="container">
              <div class="card-deck mb-3 text-center">
                <div class="card mb-4 box-shadow">
                  <div class="card-header">
                    <h1 class="my-0 font-weight-normal">Ver citas atendidas</h1>
                  </div>
                  <div class="serviceBox">
                    <div class="service-icon"><i class="fa fa-hospital-o" aria-hidden="true"></i></div>
                    <div class="card-body">
                      
                      
                      <button class="btn btn-lg btn-block btn-outline-primary" name="vercitas">Acceder</button>
                    </div>
                  </div>
                </div>

                <div class="card mb-4 box-shadow">
                  <div class="card-header">
                    <h1 class="my-0 font-weight-normal">Nueva cita</h1>
                  </div>
                  <div class="serviceBox">
                    <div class="service-icon"><i class="fa fa-medkit" aria-hidden="true"></i></div>
                    <div class="card-body">
                      
                      
                      <button class="btn btn-lg btn-block btn-outline-primary" name="nuevacita">Acceder</button>
                    </div>
                  </div>
                </div>

                <div class="card mb-4 box-shadow">
                  <div class="card-header">
                    <h1 class="my-0 font-weight-normal">Alta paciente</h1>
                  </div>
                  <div class="serviceBox">
                    <div class="service-icon"><i class="fa fa-heart" aria-hidden="true"></i></div>
                    <div class="card-body">
                      
                      
                      <button class="btn btn-lg btn-block btn-outline-primary" name="nuevopaciente">Acceder</button>
                    </div>
                  </div>
                </div>

                 <div class="card mb-4 box-shadow">
                  <div class="card-header">
                    <h1 class="my-0 font-weight-normal">Ver pacientes</h1>
                  </div>
                  <div class="serviceBox">
                    <div class="service-icon"><i class="fa fa-wheelchair" aria-hidden="true"></i></div>
                    <div class="card-body">
                      
                      
                      <button class="btn btn-lg btn-block btn-outline-primary" name="verpaciente">Acceder</button>
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="centro">
                <button class="action-button shadow animate red"  name="salir">Cerrar sesión</button>
              </div>
              
            </div>
          </div>
      </div>
    </form>
<!-- Final del panel de control del administrador --> 

<?php

  }

  if (isset($_POST['vercitas']) || isset($_POST['nuevacita']) || (isset($_POST["nuevopaciente"])) || (isset($_POST["verpaciente"])) || isset($_POST['anadircita']) || isset($_POST['anadirpaciente'])){  /* Modulos para las diferentes opciones del asistente */

      /* Conexion base de datos */

      $servername = "localhost";
      $username = "Asistente";
      $password = "Asistente";
      $dbname = "consultas";

      $conn = mysqli_connect($servername, $username, $password,$dbname);

      if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
      
      }
    
      if (isset($_POST['vercitas'])){ /* Modulo para ver las citas atendidas */

        $sql30 = " SELECT citas.citFecha, citas.citHora, pacientes.pacNombres, pacientes.pacApellidos, medicos.medNombres, medicos.medApellidos, consultorios.conNombre, citas.citEstado, citas.CitObservaciones FROM citas INNER JOIN pacientes ON citas.citPaciente= pacientes.dniPac INNER JOIN medicos ON citas.citMedico=medicos.dniMed INNER JOIN consultorios ON citas.citConsultorio= consultorios.idConsultorio WHERE citas.citEstado= 'Atendido'" ;
        
        $result30 = mysqli_query ($conn, $sql30);

?>

        <div class="container">
        <div class="well form-horizontal"  id="contact_form">
        <fieldset>

          <center><h2><b>Lista de citas atendidas</b></h2></center><br>
          <table>
            <thead>
              <tr>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Paciente</th>
                <th>Medico</th>
                <th>Consultorio</th>
                <th>Estado</th>
                <th>Observaciones</th>
              </tr>
            </thead>
            <tbody>
<?php
          while ($registro30 = mysqli_fetch_row($result30)) {
?>
              <tr>
                <td><?php echo $registro30[0];?></td>
                <td><?php echo $registro30[1];?></td>
                <td><?php echo $registro30[2];?> <?php echo $registro30[3];?></td>
                <td><?php echo $registro30[4];?> <?php echo $registro30[5];?></td>
                <td><?php echo $registro30[6];?></td>
                <td><?php echo $registro30[7];?></td>
                <td><?php echo $registro30[8];?></td>
              </tr>
<?php
          }
?>

            </tbody>
          </table>
          <form action="" method="post">
                    <div class="col-md-4"><br>
                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                  </div>
              </form>
            </div>
        <fieldset>
        </div>  
        </div>

<?php
      } /* Final del modulo para ver las citas atendidas */

      if (isset($_POST['nuevacita']) || isset($_POST['anadircita'])){ /* Modulo para crear una nueva cita */

?>      

<!-- Formulario para añadir una nueva cita --> 

            <div class="container">
            <div class="well form-horizontal"  id="contact_form">
            <fieldset>
            <form action="" method="post" >
             
                <br><br>
              <center><h2><b>Nueva cita</b></h2></center><br>

               <div class="form-group"> 
                <label class="col-md-4 control-label">Paciente</label>
                  <div class="col-md-4 selectContainer">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <select name="pac" class="form-control selectpicker" required>
                    <option value="">Seleccione</option>
<?php
                      $pacientes = "SELECT dniPac,pacNombres,pacApellidos FROM pacientes";
                      $result = mysqli_query ($conn, $pacientes);
                      while ($registro = mysqli_fetch_row($result)) {
                        echo '<option value="'.$registro[0].'">'.$registro[0]. " - ".$registro[1]." ".$registro[2].'</option>';
                      }
?>
                  </select>
                </div>
              </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label">Fecha</label>  
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                <input  name="fecha"class="form-control" type="date" value="0000-00-00" min="1900-01-01" max="2100-12-31" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label">Hora</label>  
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                <input  name="hora"class="form-control" type="time" value="08:30:00" max="15:30:00" min="08:30:00" step="1" required>
                  </div>
                </div>
              </div>

              <div class="form-group"> 
                <label class="col-md-4 control-label">Medico</label>
                  <div class="col-md-4 selectContainer">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-plus-sign"></i></span>
                  <select name="med" class="form-control selectpicker" required>
                    <option value="">Seleccione</option>
<?php
                      $medicos = "SELECT dniMed,medNombres,medApellidos,medEspecialidad FROM medicos";
                      $result1 = mysqli_query ($conn, $medicos);
                      while ($registro1 = mysqli_fetch_row($result1)) {
                        echo '<option value="'.$registro1[0].'">'.$registro1[3]. " - ".$registro1[1]." ".$registro1[2].'</option>';
                      }
?>
                  </select>
                </div>
              </div>
              </div>

                <div class="form-group"> 
                <label class="col-md-4 control-label">Consultorio</label>
                  <div class="col-md-4 selectContainer">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                  <select name="consul" class="form-control selectpicker" required>
                    <option value="">Seleccione</option>
<?php
                      $consultorio = "SELECT idConsultorio,conNombre FROM consultorios";
                      $result2 = mysqli_query ($conn, $consultorio);
                      while ($registro2 = mysqli_fetch_row($result2)) {
                        echo '<option value="'.$registro2[0].'">'.$registro2[1].'</option>';
                      }
?>
                  </select>
                </div>
              </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4"><br>
                   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="anadircita" class="btn btn-success" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAÑADIR <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
                 
              </div><br>
              
<?php    

          if (isset($_POST['anadircita'])){

            $pac=$_POST['pac'];
            $fecha=$_POST['fecha'];
            $hora=$_POST['hora'];
            $med=$_POST['med'];
            $consul=$_POST['consul'];
            
            $sql1 = "INSERT INTO citas (idCita,citFecha,citHora,citPaciente,citMedico,citConsultorio,citEstado) VALUES (NULL, '$fecha', '$hora', '$pac', '$med', '$consul', 'Asignado')";

              if ((mysqli_query($conn, $sql1))) {
                
?>
                  <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Se ha añadido la cita con exito.</div> <!-- Mensaje de exito si se ha añadido a la base de datos -->

                    
<?php 
              }else{

                echo "<br><p> Error al añadir una cita </p>";
              }
            
          }

?>             
            </form>
              <form action="" method="post">
                    <div class="col-md-4"><br>
                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                  </div>
              </form>
            <fieldset>
            </div>  
            </div>

<!--Final del formulario para crear una nueva cita --> 

<?php               

      }

      if (isset($_POST['nuevopaciente']) || isset($_POST['anadirpaciente'])){ /* Modulo para dar de alta un nuevo paciente */

          $apelErr=$nameErr = $passError = "";
          $apel=$name =$contra="";
          $apel1=$name1=$contra3=false;


        if (isset($_POST["anadirpaciente"])){ /* Validaciones del formulario que añade un nuevo paciente */

          $contra1=$_POST["pass"];
          $contra2=$_POST["rpass"];

          
            if ($contra1!=$contra2) {

              $passError = "* Las contraseñas no coinciden";

            }else{

               $contra3 = true;
            }


              $name = test_input($_POST["nombre"]);

              if (!preg_match("/^[a-zA-Z]*$/",$name)){

                  $nameErr = "<br>* Solo se permiten letras en el nombre";

              }else{

                  $name1=true;
              }

              if (strlen($name) > 12 ){

                  $nameErr = "<br>* El nombre no puede exceder los 12 caracteres";

              }else{

                  $name1=true;
              }


               $apel = test_input($_POST["apellidos"]);

              if (strlen($apel) > 30 ){

                  $apelErr = "<br>* Los apellidos no pueden exceder los 30 caracteres";

              }else{

                  $apel1=true;
              }       
        }   /* Final de las validaciones */ 

?>    

<!-- Formulario para añadir un nuevo paciente --> 

            <div class="container">
            <div class="well form-horizontal"  id="contact_form">
            <fieldset>
            <form action="" method="post" >
             
                <br><br>

              <center><h2><b>Alta de paciente</b></h2></center><br>

              <div class="form-group">
                <label class="col-md-4 control-label">DNI</label>  
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input  name="dni" placeholder="DNI" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label" >Nombre</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input name="nombre" placeholder="Nombre" class="form-control"  type="text" required>
                  </div>
                </div>
              </div>

               <div class="form-group">
                <label class="col-md-4 control-label" >Apellidos</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input name="apellidos" placeholder="Apellidos" class="form-control"  type="text" required>
                  </div>
                </div>
              </div> 

              <div class="form-group">
                <label class="col-md-4 control-label">Fecha</label>  
                <div class="col-md-4 inputGroupContainer">
                <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                <input  name="fecha"class="form-control" type="date" value="0000-00-00" min="1900-01-01" max="2100-12-31" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label" >Contraseña</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-cog"></i></span>
                <input name="pass" placeholder="Contraseña" class="form-control"  type="password" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label" >Confirmar contraseña</label> 
                  <div class="col-md-4 inputGroupContainer">
                  <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-cog"></i></span>
                <input name="rpass" placeholder="Confirmar contraseña" class="form-control"  type="password" required>
                  </div>
                </div>
              </div>

              <div class="form-group"> 
                <label class="col-md-4 control-label">Sexo</label>
                  <div class="col-md-4 selectContainer">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                  <select name="sex" class="form-control selectpicker" required>
                    <option value="">Selecciona el sexo del paciente</option>
                    <option value="Masculino">Masculino</option>
                    <option value="Femenino">Femenino</option>
               
                  </select>
                </div>
              </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4"><br>
                   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="anadirpaciente" class="btn btn-success" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspAÑADIR <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
                 
              </div><br>
              <span class="error"> <?php echo $passError;?></span><span class="error"> <?php echo $nameErr;?></span><span class="error"> <?php echo $apelErr;?></span></p><br>
    
<?php    

        if(($apel1==true) && ($name1==true) && ($contra3==true)){ /* Si todas las validaciones son aceptadas procede a trabajar con la base de datos */

          if (isset($_POST['anadirpaciente'])){

            $dni=$_POST['dni'];
            $nombre=$_POST['nombre'];
            $apellidos=$_POST['apellidos'];
            $pass=$_POST['pass'];
            $fecha=$_POST['fecha'];
            $sexo=$_POST['sex'];

            $sql9 = "SELECT dniUsu FROM usuarios WHERE dniUsu = '$dni'";
        
            $result6 = mysqli_query ($conn, $sql9);

            if(mysqli_num_rows($result6) > 0){

              echo "<span class='error'>* Usuario ya existente.</span";

            }else{
            
              $sql10 = "INSERT INTO pacientes (dniPac, pacNombres, pacApellidos, pacFechaNacimiento, pacSexo) VALUES ('$dni', '$nombre', '$apellidos', '$fecha', '$sexo')";
              $sql20 = "INSERT INTO usuarios (dniUsu, usuLogin, usuPassword, usuEstado, usutipo) VALUES ('$dni', '$nombre', '$pass', 'Activo', 'Paciente')";

              if ((mysqli_query($conn, $sql10)) && (mysqli_query($conn, $sql20))) {
                
?>
                  <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Se ha añadido el paciente con exito.</div> <!-- Mensaje de exito si se ha añadido a la base de datos -->

                    
<?php 
              }else{

                echo "<br><p> Error al añadir un nuevo paciente </p>";

              }
            }
          }
        }

?>            
            </form>
            <form action="" method="post">
                  <div class="col-md-4"><br>
                   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
            </form>
          </div>
           <fieldset>
            </div>  
            </div>

<!--Final del formulario para añadir un nuevo paciente --> 

<?php               

      } /* Final del modulo que añade nuevos pacientes */

      if (isset($_POST['verpaciente'])){ /* Modulo para ver los paciente */

        $sql60 = " SELECT * FROM pacientes";   

        $result60 = mysqli_query ($conn, $sql60);

?>

        <div class="container">
        <div class="well form-horizontal"  id="contact_form">
        <fieldset>

          <center><h2><b>Lista de pacientes</b></h2></center><br>
          <table>
            <thead>
              <tr>
                <th>DNI</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Fecha de nacimiento</th>
                <th>Sexo</th>
              </tr>
            </thead>
            <tbody>
<?php
          while ($registro60 = mysqli_fetch_row($result60)) {
?>
              <tr>
                <td><?php echo $registro60[0];?></td>
                <td><?php echo $registro60[1];?></td>
                <td><?php echo $registro60[2];?></td>
                <td><?php echo $registro60[3];?></td>
                <td><?php echo $registro60[4];?></td>
              </tr>
<?php
          }
?>
            
            </tbody>
          </table>
          <form action="" method="post">
                    <div class="col-md-4"><br>
                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="anadirpaciente" name="panel" class="btn btn-primary" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPANEL DE CONTROL <span></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                  </div>
              </form>
            </div>
        <fieldset>
        </div>  
        </div>

<?php
      } /* Final del modulo para ver los pacientes */
      mysqli_close($conn);
  }
}

?>
  




  <!-- ALL JS FILES -->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
    <!-- ALL PL../UGINS -->
  <script src="../js/jquery.magnific-popup.min.js"></script>
    <script src="../js/jquery.pogo-slider.min.js"></script> 
  <script src="../js/slider-index.js"></script>
  <script src="../js/smoothscroll.js"></script>
  <script src="../js/TweenMax.min.js"></script>
  <script src="../js/main.js"></script>
  <script src="../js/owl.carousel.min.js"></script>
  <script src="../js/form-validator.min.js"></script>
    <script src="../js/contact-form-script.js"></script>
  <script src="../js/isotope.min.js"></script> 
  <script src="../js/images-loded.min.js"></script>  
    <script src="../js/custom.js"></script>

    
</body>
</html>